This project was an end-of-year assignment for my CSCIB150 - Software Design & Development Course (Fall Semester - 2019).


Brief Overview of Project: 3 questions were posed to CompSci students by our assigned nursing student and it was up to the programmers to pull the right
questions from the spreadsheet. The program works by pulling extensive survey data that is downloaded from the CDC's (Center for Disease Control)
website. Then, it organizes the data into cell arrays that specifically correspond to the nursing students questions, and then data 
is later used for data analysis (pie charts, correlations, etc.) 

To see a more comprehensive view of the project and the program I wrote, myself, download and view the .docx file 'B104_Project_Report_Kaufman.docx'

Attached to this Bitbucket repo is: 

	B104_Project_Report_Kaufman.docx	(Project Overivew//Word Document)
	CSCIB104_Project_Data.xlsx		(CDC Downloaded Survey Responses//Excel Document)
	CSCIB104_Project_Script.m		(MATLAB code written to mangage and analyze project data//MATLAB Document)