% Programmer: Brodie Kaufman 
% CSCI/ISAT B104 ~ Fall 2019
% Assignment: Matlab_Nursing_Project.m
% 11/12/2019   

% Interdisciplinary Computer Science - Nursing Research Team 9
% Brodie Kaufman, Aleigha Pound

%% Matlab_Nursing_Project.m
clear
clc
close all

%% Intialized Variables
    Race_0_Yes = [0,0,0,0];
    Race_0_No = [0,0,0,0];
    Race_1_Yes = [0,0,0,0];
    Race_1_No = [0,0,0,0];
    Race_2_Yes = [0,0,0,0];
    Race_2_No = [0,0,0,0];
    

%% Data Read
[~,Data,All_Data] = xlsread('CSCIB104_Project_Data','Query_Project');
Text_Questions = Data(1,:);
Data(1,:) = [];

%%
All_Data(1,:) = [];
rnew = cellfun(@str2num,All_Data(:,2:3));
r1 = cell2mat(All_Data(:,1));
cdc_data = cat(2,r1,rnew);


%% Programmer Input

[~,cdc_data_length] = size(cdc_data);

for kk = 1:cdc_data_length        % loop that find unique number of responses 
a{kk} = unique(cdc_data(:,kk));   % a person can give per each question type 
                                  % e.g. (A,C,C,D,E,F,etc.)
Total_Types_Answers = (cellfun(@length,a,'uniformoutput',false));
end

%% Empty Cell Arrays
for Main_Index=1:length(Text_Questions)
    
    
    EmptyArrays{Main_Index} = cell(1,Total_Types_Answers{Main_Index});  %empty array 
    
    for hh = 1:Total_Types_Answers{Main_Index}
    
    EmptyArrays{Main_Index}(1,hh) = {0};    %places number of zeros based on number of answer choices in new created cell arays
    
    end

%% Age Count of Respondents

All_Answer_Choices{Main_Index} = EmptyArrays{Main_Index};

  
for aa= 1:length(cdc_data)      %% Loops for every single row of CDC Data
    element = cdc_data(aa,Main_Index);   %% Goes to individual elements of the column

    for bb = 1:Total_Types_Answers{Main_Index}
        if element == bb
        
        All_Answer_Choices{Main_Index}{bb} = All_Answer_Choices{Main_Index}{bb}+1 ; %counts number of times a     
        end                                                            %certain type of answer choice occurs in question column  
    end
end 
end
    
%% Frquency Count    

%% Responses by Race
    
    for ee = 1:4
        t=0;
    for aa= 1:length(cdc_data)      % Loops for every single row of CDC Data
     
    element = cdc_data(aa,1);       % Goes to individual elements of the column

        if element==ee
              t=t+1;        
        
        end
    How_Many_Race{ee} = t;                % Stores amount of respondents by race 
                                          % This is used to calculate how
                                          % many rows to go through in
                                          % cdc_data (as seen below)
    end
    end
    
    
    %% Storing Answer Choices for each race
    h=0;
    for ee = 1:4
        
    for t = 1:How_Many_Race{ee}
        
    Response_by_Race{ee}(t,1:2) = cdc_data(t+h,(2:3)); % Uses the previous variable
    
    if Response_by_Race{1,ee}(t,1)==1 && Response_by_Race{1,ee}(t,2)==1 % Sorts every answer choice by race (Frequency)
    Race_0_Yes(1,ee) = Race_0_Yes(1,ee)+1
    
    elseif Response_by_Race{1,ee}(t,1)==1 && Response_by_Race{1,ee}(t,2)==2
    Race_0_No(1,ee) = Race_0_No(1,ee)+1
    
    elseif Response_by_Race{ee}(t,1)==2 && Response_by_Race{ee}(t,2)==1
    Race_1_Yes(1,ee) = Race_1_Yes(1,ee)+1;
    
    elseif Response_by_Race{ee}(t,1)==2 && Response_by_Race{ee}(t,2)==2
    Race_1_No(1,ee) = Race_1_No(1,ee)+1;
    
    elseif Response_by_Race{ee}(t,1)==3 && Response_by_Race{ee}(t,2)==1
    Race_2_Yes(1,ee) = Race_2_Yes(1,ee)+1;
    
    elseif Response_by_Race{ee}(t,1)==3 && Response_by_Race{ee}(t,2)==2
    Race_2_No(1,ee) = Race_2_No(1,ee)+1;
        
        end                                                   % " How_Many_Race "                  
    end                                                % to create a new variable which stores all
    h=h+t;                                             % responses by race
    end
    

    
%%

All_Answer_Choices = cellfun(@cell2mat,All_Answer_Choices,'UniformOutput',false); %Variable with count of all answer choices/answer count for all questions

%% Code for Pie Charts

for Pie_Index=1:length(Text_Questions)
    figure(Pie_Index);
    
    All_Pies{Pie_Index} = pie(All_Answer_Choices{Pie_Index});

    pText{Pie_Index} = findobj(All_Pies{Pie_Index},'Type','text');
    percentValues{Pie_Index} = get(pText{Pie_Index},'String'); 
    
    if Pie_Index==1    
        title('Percentage by Race of Respondents')
        txt = {'White: ';'Black or African American: '; '�Hispanic/Latino: '; 'All Other Races: '}; 
    elseif Pie_Index==2
        title('Illegal injected drug use Percentage')
        txt = {'0 Times: ';'1 Time: '; '2 or More Times: '};
    else
        title('Illegal drugs at school Percentage')
        txt = {'Yes: ';'No: '};
    end
    
    combinedtxt = strcat(txt,percentValues{Pie_Index}); 
    
for bb = 1:Total_Types_Answers{Pie_Index}
 
 pText{Pie_Index}(bb).String = combinedtxt{bb};
 
end
end

%% Correlation of all 3 Questions

cdc_data(:,4) = sum(cdc_data,2); %https://www.mathworks.com/help/matlab/ref/corrcoef.html

[r,p] = corrcoef(cdc_data);
fprintf('Correlation Coefficients between %s, %s, and %s\n\n',Text_Questions{1},Text_Questions{2},Text_Questions{3})
disp(r(1:cdc_data_length,1:cdc_data_length))
    